package com.example.hieuluong792.tonetesting;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;

import com.github.nkzawa.socketio.client.Socket;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.JsonReader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity {

    private final int REQ_CODE_SPEECH_INPUT = 200;
    Button btnSendMessage, send;
    TextView txtSpeechInput;
    TextView currentMood, txtAnger, txtDisgust, txtFear, txtJoy, txtSadness, txtAnalytical, txtConfident, txtTentative, txtOpenness, txtConscientiousneous, txtExtraversion, txtAgreeableness, txtEmotionalRange;
    private static String MESSAGE;
    EditText input;
    float max;
    String mood="";


    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("http://hdluong-tone-analyzer.mybluemix.net");
            //mSocket = IO.socket("http://192.168.1.16:3000");
            // mSocket = IO.socket("http://10.1.14.190:3000");
        } catch (URISyntaxException e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            mSocket.connect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mSocket.on("result-json", onNewMessage);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);

        txtAnger = (TextView) findViewById(R.id.txtAnger);
        txtDisgust = (TextView) findViewById(R.id.txtDisgust);
        txtFear = (TextView) findViewById(R.id.txtFear);
        txtJoy = (TextView) findViewById(R.id.txtJoy);
        txtSadness = (TextView) findViewById(R.id.txtSadness);
        txtAnalytical = (TextView) findViewById(R.id.txtAnalytical);
        txtConfident = (TextView) findViewById(R.id.txtConfident);
        txtTentative = (TextView) findViewById(R.id.txtTentative);
        txtOpenness = (TextView) findViewById(R.id.txtOpenness);
        txtConscientiousneous = (TextView) findViewById(R.id.txtConscientiousneous);
        txtExtraversion = (TextView) findViewById(R.id.txtExtraversion);
        txtAgreeableness = (TextView) findViewById(R.id.txtAgreeableness);
        txtEmotionalRange = (TextView) findViewById(R.id.txtEmotionalRange);
     //   currentMood = (TextView) findViewById(R.id.currentMood);

        input = (EditText) findViewById(R.id.inputText);
        send = (Button) findViewById(R.id.send);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mSocket.emit("send-message", input.getText().toString());


                insert("https://emotionly.000webhostapp.com/API/insert.php");


            }
        });


        input.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Abstract Method of TextWatcher Interface.
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
                // Abstract Method of TextWatcher Interface.
            }

            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {
                System.out.println(s);

             //   mSocket.emit("send-message", input.getText().toString());


                if (!s.toString().isEmpty())
                    if (s.toString().charAt(s.length() - 1) == ' ') {
                        mSocket.emit("send-message", input.getText().toString());


                    }


            }
        });


        btnSendMessage = (Button) findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject root = (JSONObject) args[0];
                    JSONObject jsonFile;
                    try {
                        jsonFile = new JSONObject(root.getString("jsonFile"));

                        JSONObject document_tone = new JSONObject(jsonFile.getString("document_tone"));

                        JSONArray tone_categories = new JSONArray(document_tone.getString("tone_categories"));

                        for (int i = 0; i < tone_categories.length(); i++) {
                            JSONObject sub_category = tone_categories.getJSONObject(i);
                            JSONArray tones = new JSONArray(sub_category.getString("tones"));


                            for (int j = 0; j < tones.length(); j++) {
                                JSONObject sub_tone = tones.getJSONObject(j);
                                if (i == 0) {

                                  if(max< Float.parseFloat(sub_tone.getString("score"))){
                                      max=Float.parseFloat(sub_tone.getString("score"));

                                      Log.d("max",String.valueOf(max));
                                    }



                                    switch (j) {
                                        case 0:
                                            txtAnger.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtDisgust.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtFear.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtJoy.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtSadness.setText(sub_tone.getString("score"));

                                            break;
                                    }
                                } else if (i == 1) {
                                    switch (j) {
                                        case 0:
                                            txtAnalytical.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConfident.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtTentative.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                } else {
                                    switch (j) {
                                        case 0:
                                            txtOpenness.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConscientiousneous.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtExtraversion.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtAgreeableness.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtEmotionalRange.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                }
                            }

                        }

                        JSONObject sub_category = tone_categories.getJSONObject(0);
                        JSONArray tones = new JSONArray(sub_category.getString("tones"));

                        for (int j = 0; j < tones.length(); j++) {
                            JSONObject sub_tone = tones.getJSONObject(j);



                                    if(max==Float.parseFloat(sub_tone.getString("score"))){

                                        System.out.println(sub_tone);
                                        System.out.println(max);


                                        if(j==0){
                                            mood="anger";
                                        }
                                        else if(j==1){
                                            mood="disgust";
                                        }
                                        else if(j==2){
                                            mood="fear";

                                        }
                                        else if(j==3){
                                            mood="joy";
                                        }
                                        else if(j==4){
                                            mood="sadness";
                                        }

                                        max=0;

                                    }




                            }

                        Log.d("mood",mood);




                        //String tone_categories = document_tone.getString("tone_categories");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(resultCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    MESSAGE = result.get(0);
                    mSocket.emit("send-message", MESSAGE);
                }
                break;
            }
        }
    }

    public boolean OnCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void insert(String url) {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String URL = url;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Successfully Inserted",
                                Toast.LENGTH_SHORT);

                        toast.show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Could not Insert",
                                Toast.LENGTH_SHORT);

                        toast.show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("text", input.getText().toString() );
                params.put("anger", txtAnger.getText().toString());
                params.put("joy", txtJoy.getText().toString());
                params.put("disgust", txtDisgust.getText().toString());
                params.put("sadness", txtSadness.getText().toString());
                params.put("fear", txtFear.getText().toString());
                params.put("mood", mood);

                return params;
            }
        };

        requestQueue.add(postRequest);
    }
}